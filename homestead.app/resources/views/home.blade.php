<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @yield('aimeos_header')
  <title>EMQ | Home</title>
  <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
  @yield('aimeos_styles')
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">EMQ</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{ url('/list') }}">Products</a></li>
        <li><a href="{{ url('/stores') }}">Stores</a></li>
        <li><a href="{{ url('/tracking') }}">Tracking</a></li>
        <li><a href="{{ url('/about') }}">About Us</a></li>
        <li><a href="{{ url('/contact') }}">Contact</a></li>
      </ul>

      <div class="nav navbar-nav navbar-right">



        <!-- Authentication Links -->
        @if (Auth::guest())
          <li><a href="{{ url('/login') }}">Login</a></li>
          <li><a href="{{ url('/register') }}">Register</a></li>
        @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i><h4>Logout</h4></a></li>
              <li><a href="{{ url('/basket') }}"><i class="fa fa-btn"></i><h4>My Cart</h4></a></li>
            </ul>
          </li>
        @endif

      </div>
    </div>
  </div>
</nav>

  <div class="col-xs-2"></div>

  <div class="container col-xs-8">

    <div class="row jumbotron text-center">
      <h1>Welcome to EMQ's Online Store</h1>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">EMQ is now online! <span class="text-muted">It's amazing</span></h2>
        <p class="lead">What started out as a simple mom-and-pop shop has turned into one of the largest electronic store chains in all of California!</p>
        <p class="lead">We are built to emphasis a positive buyer experience through our entire pipeline.</p>
        <p class="lead">Whether you prefer an in person buying experience or like to order from your comfortable couch, we've got you covered!</p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" style="height: 30rem;" src="images/EMQ-Logo-1.png" alt="Generic placeholder image">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 col-md-push-5">
        <h2 class="featurette-heading">Don't like the internet? <span class="text-muted">Come to one of our locations!</span></h2>
        <p class="lead">If you prefer a more personal approach to electronic shopping come on into any one of our many locations!</p>
        <p class="lead">Each store has identical stock and is filled with motivated EMQ tech experts ready to help you.</p>
      </div>
      <div class="col-md-5 col-md-pull-7">
        <img class="featurette-image img-responsive center-block" style="box-shadow: 1px 1px 20px 5px grey;" src="images/EMQ-Map-1.png" alt="Generic placeholder image">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">It's easy to buy. <span class="text-muted" style="font-size: 2rem;">(about time)</span></h2>
        <p class="lead">From adding products to your cart to checking the estimated delivery times, buying through EMQ is as easy as can be!</p>
        <p class="lead">If you still need some help feel free to contact us <a href="{{ url('/contact') }}">here</a></p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" style="box-shadow: 1px 1px 20px 5px grey;" src="images/EMQ-Products-1.png" alt="Image of product page">
      </div>
    </div>
    <footer class="footer text-center" style="padding: 1em">2016 EMQ Tech Team&reg</footer>
  </div>

  <!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
  @yield('aimeos_scripts')
</body>
</html>