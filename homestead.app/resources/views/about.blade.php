<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('aimeos_header')
    <title>EMQ | About</title>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    @yield('aimeos_styles')
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">EMQ</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/list') }}">Products</a></li>
                <li><a href="{{ url('/stores') }}">Stores</a></li>
                <li><a href="{{ url('/tracking') }}">Tracking</a></li>
                <li><a href="{{ url('/about') }}">About Us</a></li>
                <li><a href="{{ url('/contact') }}">Contact</a></li>
            </ul>

            <div class="nav navbar-nav navbar-right">


                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i><h4>Logout</h4></a>
                            </li>
                            <li><a href="{{ url('/basket') }}"><i class="fa fa-btn"></i><h4>My Cart</h4></a></li>
                        </ul>
                    </li>
                @endif

            </div>
        </div>
    </div>
</nav>

<div class="col-xs-2"></div>

<div class="container col-xs-8">

    <div class="row featurette text-center">
        <div class="col-md-12">
            <h1 class="featurette-heading">About Us</h1>
            <br>
            <h3>Who are we and what do we do?</h3>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row text-center">
        <div class="col-lg-4">
            <img class="img-circle"
                 src="https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAY5AAAAJDRhOTk2ZjUxLTE3YzEtNGRlMC04OTczLTU4ZmZhMWIyNmM2NQ.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>Victor Fateh</h2>
            <p>A 4th year Computer Science student at San Jose State University who's passionate about the mobile world.
                Victor spends his free time building dynamic sites and mobile applications.</p>
            <p><a class="btn btn-info" target="_blank" href="https://www.linkedin.com/in/victorfateh" role="button">LinkedIn &raquo;</a>
            </p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle"
                 src="https://media.licdn.com/media/AAEAAQAAAAAAAAO6AAAAJDg0OWVlNWExLWI5MDktNGVhYy1hYzcwLWU2MDkzOTg5NWQzMA.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>Kyle Escott</h2>
            <p>Sixth year Computer Science student studying at SJSU, on track to graduate in
                May 2017. Proficient in Java, C, and C++ coding languages. Also likes Corgis.</p>
            <p><a class="btn btn-info" href="http://www.linkedin.com/in/kyle-escott-16255b105"
                  role="button">LinkedIn &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle"
                 src="https://media.licdn.com/media/AAEAAQAAAAAAAAbBAAAAJGIxYTI1MWQ3LTA4MzctNGI4ZS04YWQ3LTFkZDgwYThhMzgwYg.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>Bryan Garcia</h2>
            <p>Fifth year Computer Science student graduating in May 2017. Specializing in Software Development and User
                Interfaces. Passionate to learn!</p>
            <p><a class="btn btn-info" target="_blank" href="https://www.linkedin.com/in/bryangarcia831" role="button">LinkedIn &raquo;</a>
            </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row text-center">
        <div class="col-lg-4">
            <img class="img-circle"
                 src="https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAT2AAAAJDkxNDhiM2U5LTBkMTYtNDdiNi1hMTU0LWUzYWE2NzNjZWNkNw.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>Ricardo Camarena</h2>
            <p>Computer Science student graduating in May 2017. Interests include mobile development, virtual reality,
                and puppies.</p>
            <p> "Take a byte out of life"</p>
            <p><a class="btn btn-info" href="https://www.linkedin.com/in/ricardo-camarena-514110105" role="button">LinkedIn &raquo;</a>
            </p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle" src="http://fabriccreative.com/wp-content/uploads/2014/05/grace-face-1.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>David Barban</h2>
            <p>Duis amollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras
                mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                condimentum nibh.</p>
            <p><a class="btn btn-info" href="#" role="button">LinkedIn &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle" src="http://fabriccreative.com/wp-content/uploads/2014/05/grace-face-1.jpg"
                 alt="Generic placeholder image" width="140" height="140">
            <h2>Frank Butt</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-info" href="#" role="button">LinkedIn &raquo;</a></p>
        </div>

        <div class="col-xs-2"></div>

        <!-- Scripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
@yield('aimeos_scripts')
</body>
</html>