<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	@yield('aimeos_header')
	<title>EMQ | Stores</title>
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
	@yield('aimeos_styles')
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">EMQ</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="{{ url('/list') }}">Products</a></li>
				<li><a href="{{ url('/stores') }}">Stores</a></li>
				<li><a href="{{ url('/tracking') }}">Tracking</a></li>
				<li><a href="{{ url('/about') }}">About Us</a></li>
				<li><a href="{{ url('/contact') }}">Contact</a></li>
			</ul>

			<div class="nav navbar-nav navbar-right">



				<!-- Authentication Links -->
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i><h4>Logout</h4></a></li>
							<li><a href="{{ url('/basket') }}"><i class="fa fa-btn"></i><h4>My Cart</h4></a></li>
						</ul>
					</li>
				@endif

			</div>
		</div>
	</div>
</nav>

<div class="col-xs-2"></div>

<div class="container col-xs-8">

	<div class="row featurette text-center">
		<div class="col-md-12">
			<h1 class="featurette-heading">Store Locations</h1>
		</div>
	</div>

	<hr class="featurette-divider">

	<!-- var myLatLng = {lat: 37.5735774, lng: -122.1373055}; -->

	<div class="row">
		<div id="map" style="width: 100%; height: 60rem;"></div>

		<script type="text/javascript">

			function initMap() {

				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 9,
					center: {lat: 37.6, lng: -122.1373055}
				});

				setMarkers(map);

			}

			function writeMap(){
				google.maps.event.addDomListener(window, 'load', initialize);
			}

			function addMyMarker() {
				myLatlng = new google.maps.LatLng(37.6,-122.1373055);
				marker = new google.maps.Marker({position: myLatlng, map: map,title: 'Hello PLUTO!'});
			}

			var locations = [
				['EMQ San Jose', 37.322505, -121.948832, 4, '<div class="text-center">' + '<h3>EMQ - San Jose</h3>' + '<p>Located in Beautiful Santana Row. Stevens Creek Blvd and Winchester Blvd.</p>' + '<p>3090 Stevens Creek Blvd, San Jose, CA 95128</p>' + '<p><a href="http://maps.google.com/maps?daddr=37.322505,-121.948832" target="_blank">Directions in New Tab</a></p>'  + '</div>'],
				['EMQ Mountain View', 37.4234121, -122.097137, 4,'<div class="text-center">' + '<h3>EMQ - Mountain View</h3>' + '<p>Exit Regstorff on 101</p>' + '<p>2460 E Charleston Rd, Mountain View, CA 94043' +'<p><a href="http://maps.google.com/maps?daddr=37.4234121,-122.097137" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ San Carlos', 37.5023553, -122.2441435, 4, '<div class="text-center">' + '<h3>EMQ - San Carlos</h3>' + '<p>Located off 101 at Whipple Avenue.</p>' + '<p>1127 Industrial Rd, San Carlos, CA 94070' +'<p><a href="http://maps.google.com/maps?daddr=37.5023553,-122.2441435" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ Union City', 37.6011762, -122.0641994, 4, '<div class="text-center">' + '<h3>EMQ - Union City</h3>' + '<p>Union Landing Shopping Center. Exit 880 at Alvarado Niles.</p>' + '<p>31350 Courthouse Dr, Union City, CA 94587' +'<p><a href="http://maps.google.com/maps?daddr=37.6011762,-122.0641994" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ Oakland', 37.8274126, -122.2868443, 4, '<div class="text-center">' + '<h3>EMQ - Oakland</h3>' + '<p>Right before you get on the Bay Bridge. Exit 580 at Adeline St.</p>' + '<p>3700 Mandela Pkwy, Oakland, CA 94608' +'<p><a href="http://maps.google.com/maps?daddr=37.8274126,-122.2868443" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ San Francisco Downtown', 37.7684758, -122.4129295, 4, '<div class="text-center">' + '<h3>EMQ - San Francisco</h3>' + '<p>Located in the heart of Mission District. Near Folsom and 101.</p>' + '<p>1717 Harrison St, San Francisco, CA 94103' +'<p><a href="http://maps.google.com/maps?daddr=37.7684758,-122.4129295" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ San Bruno', 37.6393154, -122.420311, 4, '<div class="text-center">' + '<h3>EMQ - San Bruno</h3>' + '<p>Located on El Camino Real and Sneath Lane.</p>' + '<p>1250 El Camino Real, San Bruno, CA 94066' +'<p><a href="http://maps.google.com/maps?daddr=37.6393154,-122.420311" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ San Francisco', 37.782223, -122.446203, 4, '<div class="text-center">' + '<h3>EMQ - San Francisco</h3>' + '<p>Located at the corner of Geary Blvd and Masonic Ave.</p>' + '<p>2675 Geary Blvd #300, San Francisco, CA 94118' +'<p><a href="http://maps.google.com/maps?daddr=37.782223, -122.446203" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ Pinole', 37.991814, -122.306913, 4, '<div class="text-center">' + '<h3>EMQ - Pinole</h3>' + '<p>Pinole Vista Shopping Center in Pinole.</p>' + '<p>1490 Fitzgerald Dr, Pinole, CA 94564' +'<p><a href="http://maps.google.com/maps?daddr=37.991814, -122.306913" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
				['EMQ Pleasant Hill', 37.935144, -122.059241, 4, '<div class="text-center">' + '<h3>EMQ - Pleasant Hill</h3>' + '<p>Just off 680 on Buskirk Ave in Pleasant Hill.</p>' + '<p>3260 Buskirk Ave, Pleasant Hill, CA 94523' +'<p><a href="http://maps.google.com/maps?daddr=37.935144, -122.059241" target="_blank">Directions in New Tab</a></p>'  +  '</div>'],
			];



			function getDistance(currLocation){
				var distances = [];
				for (var i = 0; i < 8; i++){

					var destLocation = new google.maps.LatLng({lat: (locations[i])[1], lng: (locations[i])[2]});

					distances[i] = google.maps.geometry.spherical.computeDistanceBetween(currLocation, destLocation);

				}
				var lowest = [distances[0], 0];

				for (var i = 1; i < 7; i++){
					if (distances[i] < lowest[0]){
						lowest = [distances[i], i];
					}

				}
				return lowest[1];
			}


			function setMarkers(map) {

				for (var i = 0; i < locations.length; i++) {
					var location = locations[i];
					var infowindow = new google.maps.InfoWindow({
						content: location[4]
					});
					var marker = new google.maps.Marker({
						position: {lat: location[1], lng: location[2]},
						content: location[4],
						map: map,
						label: '',
						zIndex: location[3],
						icon: 'http://maps.google.com/mapfiles/ms/micons/red.png'
					});

					var circle = new google.maps.Circle({
						marker: marker,
						map:map,
						radius:8500,
						fillOpacity: 0.18,
						strokeOpacity: 0,
						animation: google.maps.Animation.DROP,
						fillColor: "red"
					});

					circle.bindTo('center',marker,'position');

					google.maps.event.addListener(circle, 'click', function () {
						infowindow.setContent(this.marker.content);
						infowindow.open(map, this.marker);
					});

					google.maps.event.addListener(marker, 'click', function () {
						infowindow.setContent(this.content);
						infowindow.open(map, this);
						map.setCenter(this.position);
					});
				}

			}


		</script>

		<p class="text-center" style="margin-top: 1rem;">Click on a marker for more info</p>

		<br>

	</div>

	<hr class="featurette-divider" style="margin-top: 0rem;">

	<footer class="footer text-center" style="padding: 1em">2016 EMQ Tech Team&reg</footer>
</div>

<div class="col-xs-2"></div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9sFmgmI5R1fhhS8ICbVCz0v98EwMlzlI&callback=initMap" async defer></script>
@yield('aimeos_scripts')

</body>
</html>