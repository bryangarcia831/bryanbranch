function initMap() {


	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: {lat: 37.6, lng: -122.1373055}
	});
	setMarkers(map);
	userMarker(map);

}

function userMarker(map) {

}

function setMarkers(map) {

	for (var i = 0; i < locations.length; i++) {
		var storeNum = i + 1; 
		var location = locations[i];
		var infowindow = new google.maps.InfoWindow({
			content: location[4]
		});
		var marker = new google.maps.Marker({
			position: {lat: location[1], lng: location[2]},
			animation: google.maps.Animation.DROP,
			content: location[4],
			map: map,
			zIndex: location[3],
			icon: '/images/computer-icon.png'
		});

		var circle = new google.maps.Circle({
			map: map,
			radius: 9000,
			fillColor: '#97884e',
			strokeOpacity: 0,
			animation: google.maps.Animation.DROP
		});

		circle.bindTo('center', marker, 'position');

		google.maps.event.addListener(marker, 'click', function () {
			infowindow.setContent(this.content);
			infowindow.open(map, this);
		});
	}

	var myMarker = new google.maps.Marker({
		position: {lat: 37.596353, lng: -122.193929},
		map: map
	});

	console.log('hello');

}

/*
Hard Coded Table array of all stores
*/
var locations = [
['EMQ 1', 37.3393939,-121.9346238, 4, '<div class="text-center">' + '<h3>EMQ Store 1</h3>' + '<p>This is a description of store number 1.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 2', 37.4234121, -122.097137, 4,'<div class="text-center">' + '<h3>EMQ Store 2</h3>' + '<p>This is a description of store number 2.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 3', 37.5023553, -122.2441435, 4, '<div class="text-center">' + '<h3>EMQ Store 3</h3>' + '<p>This is a description of store number 3.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 4', 37.6011762, -122.0641994, 4, '<div class="text-center">' + '<h3>EMQ Store 4</h3>' + '<p>This is a description of store number 4.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 5', 37.8274126, -122.2868443, 4, '<div class="text-center">' + '<h3>EMQ Store 5</h3>' + '<p>This is a description of store number 5.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 6', 37.7684758, -122.4129295, 4, '<div class="text-center">' + '<h3>EMQ Store 6</h3>' + '<p>This is a description of store number 6.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 7', 37.6393154, -122.420311, 4, '<div class="text-center">' + '<h3>EMQ Store 7</h3>' + '<p>This is a description of store number 7.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
['EMQ 7', 37.6990682, -122.1262765, 4, '<div class="text-center">' + '<h3>EMQ Store 7</h3>' + '<p>This is a description of store number 7.</p>' + '<p>Here is another part of the description, maybe with an address?' + '</div>'],
];
