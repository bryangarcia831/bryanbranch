<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\OrderStatus;
use App\Http\Requests;


class TrackingController extends Controller
{

    public function postFunction(Request $request)
    {

        if ($request->isMethod('post')) {
            $userName = $request->input('accountID');
        }

        $order = Order::where([['email', '=', $userName],['type', '=', 'payment']])
            ->select('baseid', 'address1', 'postal', 'city')
            ->first();

        if ($order == null) {
            return view('tracking', ['orderStatus' => ("There is no order for  " . $userName), 'userAddress'=>"none"]);
        }

        $stringUserAddress = $order->address1 . " ". $order->city . " " . $order->postal;

        $orderStatus = OrderStatus::where('baseid', '=', $order->baseid)
            ->select('statusdelivery', 'datepayment')
            ->first();

        $check = $this->checkItemStatus($orderStatus->datepayment);

        $stringOrderStatus = "";
        if ($check == -1) {
            $stringOrderStatus = "We are still processing your order!";
        } else if ($check == 0) {
            $orderStatus->statusdelivery = 0;
            $orderStatus->save();
            $stringOrderStatus = "We are now delivering your order!";

        } else if ($check == 1) {
            $orderStatus->statusdelivery = 1;
            $orderStatus->save();
            $stringOrderStatus = "You should have received your item!";
        }

        return view('tracking', ['orderStatus' => $stringOrderStatus,
            'userAddress'=>$stringUserAddress]);

    }


    private function checkItemStatus($timeOrdered)
    {

        $timeDateOrdered = new \DateTime($timeOrdered);
        $currentDate = new \DateTime();

        $minutesSince = abs($currentDate->getTimestamp() - $timeDateOrdered->getTimestamp()) / 60;


        if ($minutesSince < 60) {
            return -1;
        } else if ($minutesSince < 120) {
            return 0;
        } else if ($minutesSince > 121){
            return 1;
        }
        return -1;

    }


}
