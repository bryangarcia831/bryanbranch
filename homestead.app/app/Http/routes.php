<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/logout', function () {
	return view('home');
});

Route::get('/about', function() {
	return view('about');
});

Route::get('/stores', function() {
    return view('stores');
});

Route::get('/contact', function() {
    return view('contact');
});

Route::get('/tracking', function() {
    return view('tracking');
});

Route::post('/tracking', 'TrackingController@postFunction');

//Route::get('',function (){
//    return view('tracking', $orderId);
//});


Route::auth();
